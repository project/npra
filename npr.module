<?php
/**
 * @file
 * Module to allow alternative way to rebuild node permissions.
 */

/**
 * Implements hook_menu_alter().
 */
function npr_menu_alter(&$items) {
  $items['admin/reports/status/rebuild'] = array(
    'title' => 'Rebuild Permissions',
    'description' => 'Rebuild permissions the alternative way.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('npr_permission_form'),
    'access callback' => ' user_access',
    'access arguments' => array('rebuild permissions'),
    'file' => drupal_get_path('module', 'npr') . '/admin/npr.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function npr_permission() {
  return array(
    'rebuild permissions' => array(
      'title' => t('Rebuild permissions'),
    ),
  );
}

/**
 * Retrieves the grants for a node.
 *
 * @param object $node
 *   Node object that.
 *
 * @return array
 *   An array of grants for the node.
 */
function _npr_get_grants($node) {
  $grants = module_invoke_all('node_access_records', $node);
  drupal_alter('node_access_records', $grants, $node);
  if (empty($grants) && !empty($node->status)) {
    $grants[] = array(
      'realm' => 'all',
      'gid' => 0,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
    );
  }
  else {
    // Retain grants by highest priority.
    $grant_by_priority = array();
    foreach ($grants as $g) {
      $grant_by_priority[intval($g['priority'])][] = $g;
    }
    krsort($grant_by_priority);
    $grants = array_shift($grant_by_priority);
  }
  return $grants;
}

/**
 * Batch operation to collect the grants of nodes.
 *
 * The batch operation collects the node grants and batches them up together
 * to make a single write query so that the database does not get hit that
 * often.
 */
function _npr_rebuild_batch_operation($limit, $delete_limit, &$context) {
  if (empty($context['sandbox'])) {
    // Initiate multistep processing.
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->distinct()
      ->countQuery()
      ->execute()
      ->fetchField();
    $context['results'] = array();
  }
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.nid', $context['sandbox']['current_node'], '>')
    ->orderBy('n.nid', 'ASC')
    ->range(0, $limit);
  $nids = $query->execute()->fetchCol();
  $nodes = node_load_multiple($nids, array(), TRUE);
  foreach ($nodes as $node) {
    $grants = _npr_get_grants($node);
    foreach ($grants as &$grant) {
      $grant['nid'] = $node->nid;
    }
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['results'] = array_merge($context['results'], $grants);
    $context['message'] = count($context['results']);
  }
  if (count($context['results']) > $delete_limit) {
    _npr_write_data($context['results']);
    $context['results'] = array();
  }
  // Multistep processing : report progress.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  else {
    _npr_write_data($context['results']);
  }
}

/**
 * Finish callback.
 */
function _npr_rebuild_batch_finished($success, $results, $operations) {
  node_access_needs_rebuild(FALSE);
  // Delete records that are not in the node table.
  $query = db_select('node', 'n')
    ->fields('n', array('nid'));
  db_delete('node_access')
    ->condition('nid', $query, 'NOT IN')
    ->execute();
  cache_clear_all();
  drupal_set_message(t("Permissions rebuild."));
}

/**
 * Inserted the collected grants into the database.
 *
 * @param array $results
 *   Array of grants to write into database.
 */
function _npr_write_data(array $results) {
  $fields = array(
    'nid',
    'realm',
    'gid',
    'grant_view',
    'grant_update',
    'grant_delete',
  );
  $query = db_insert('node_access')->fields($fields);
  $nids = array();
  foreach ($results as &$grant) {
    $nids[] = $grant['nid'];
    $query->values($grant);
  }

  db_delete('node_access')
    ->condition('nid', $nids, 'IN')
    ->execute();
  $query->execute();
}
