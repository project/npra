<?php
/**
 * @file
 *
 * Contains administration forms and logic.
 */

/**
 * Replacement form with configurable limits for node permission rebuild form.
 */
function npr_permission_form() {
  $form['limit'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Proccess limit'),
    '#description' => t('The number of nodes that will be processed at once.'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => 100,
  );
  $form['delete_limit'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Delete limit'),
    '#description' => t(
      'The number of collected grants to run an INSERT query on.'
    ),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => 5000,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild'),
  );
  return $form;
}

/**
 * Submit handler for npr_permission_form.
 */
function npr_permission_form_submit($form, &$form_state) {
  node_access_needs_rebuild(TRUE);
  $batch = array(
    'title' => t('Rebuilding content access permissions'),
    'operations' => array(
      array(
        '_npr_rebuild_batch_operation',
        array(
          $form_state['values']['limit'],
          $form_state['values']['delete_limit'],
        ),
      ),
    ),
    'finished' => '_npr_rebuild_batch_finished',
  );
  batch_set($batch);
}
