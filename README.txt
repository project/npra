INTRODUCTION
------------
This module was created by Haymarket Media Group by DMT team.
The module allows an alternative way of rebuilding node permissions
without making the websitte content unavailable for the time of the rebuild
and reduce the load on the backend.

INSTALLATION
------------
 * Put the module directory into sites/all/modules folder in your Drupal
   installation.

 * Enable the module in admin/modules or using drush

CONFIGURATION
-------------
 * You will need the 'Rebuild permissions' to rebuild the node permissions.

 * When you need to rebuild permissions go to 'admin/reports/status/rebuild'.

 * Set the limit of the number of the nodes you want to process at a time

 * Set the limit of the grants you want to collect before writing them to
   the database.

 * Run the rebuild permissions.

MAINTAINERS
-----------
Current maintainers:
 * Mihhail Arhipov (skein) - https://drupal.org/user/784174
